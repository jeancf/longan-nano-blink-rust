Simple project to test and compare different methods to manipulate the memory-mapped registers of the Longan Nano.

1. The `raw` approach, without any external crate simply by applying the mechanisms described in the datasheet
2. Using the Peripheral Access Create (`gd32vf103-pac`) that provides descirptions and low-level access functions to all memory-mapped registers
3. using the Hardware Abstraction Layer create implementing embedded-hal (`gd32vf103xx-hal`) for a higher level interface

Use rust nightly to compile. To install:
```
> rustup install nightly
> rustup default nightly
```
Then to compile:
```
> cargo build
```
To upload using sipeed-rv-debugger you need the RISC-V fork of openocd
(`riscv-openocd-git` package on Archlinux) and the version of gdb for remote
RISC-V 32-bit targets (`riscv32-elf-gdb` package on Archlinux) , connect the
debugger to the board, insert debugger in USB port, then from the root of the repo:
start openocd
```
> riscv64-linux-gnu-openocd -f sipeed-rv-debugger.cfg
```
start gdb
```
> riscv32-elf-gdb
```
then upload one of the binary images named `raw`, `pac` or `hal`
```
(gdb) target extended-remote :3333
(gdb) monitor flash erase_sector 0 0 127
(gdb) monitor flash write_image hal

```