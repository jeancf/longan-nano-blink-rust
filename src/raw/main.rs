#![no_std]
#![no_main]

use core::ptr::{read_volatile, write_volatile};

use panic_halt as _;
use riscv_rt::entry;

// Addresses of registers
const RCU: u32 = 0x4002_1000; // RCU base address
const RCU_APB2EN: u32 = RCU + 0x18; // APB2 enable register
const RCU_APB1EN: u32 = RCU + 0x1c; // APB1 enable register

const GPIOA: u32 = 0x4001_0800; // GPIOA base address
const GPIOA_CTL0: u32 = GPIOA; // GPIOA control register
const GPIOA_BOP: u32 = GPIOA + 0x10; // Bit operate register
const GPIOA_BC: u32 = GPIOA + 0x14; // Bit clear register

const TIMER5: u32 = 0x4000_1000; // TIMER5 base address
const TIMER5_CTL0: u32 = TIMER5; // TIMER5 control register
const TIMER5_PSC: u32 = TIMER5 + 0x28; // Prescaler register
const TIMER5_CAR: u32 = TIMER5 + 0x2c; // Counter auto-reload value
const TIMER5_INTF: u32 = TIMER5 + 0x10; // Interrupt flag register

struct GreenLed {}

impl GreenLed {
    fn init() -> Self {
        // Enable GPIO port A clock
        unsafe {
            let mut rcu_apb2en = read_volatile(RCU_APB2EN as *mut u32);
            // Set bit 2 to 1
            rcu_apb2en |= 0b1 << 2;
            write_volatile(RCU_APB2EN as *mut u32, rcu_apb2en);
        }
        // Set the pin to push-pull and speed at 2 MHz
        unsafe {
            let mut gpio_ctl0 = read_volatile(GPIOA_CTL0 as *mut u32);
            // Set bits 4-7 to 0
            gpio_ctl0 &= !(0b1111 << 4);
            // Set bit 5 to 1
            gpio_ctl0 |= 0b1 << 5;
            write_volatile(GPIOA_CTL0 as *mut u32, gpio_ctl0);
        }
        GreenLed {}
    }

    // Turn LED on. LED is turned on by setting the pin to low
    fn on(&self) {
        unsafe {
            let gpioa_bc = 0b1 << 1;
            write_volatile(GPIOA_BC as *mut u32, gpioa_bc);
        }
    }

    // Turn LED off. LED is turned on by setting the pin to high
    fn off(&self) {
        unsafe {
            let gpioa_bop = 0b1 << 1;
            write_volatile(GPIOA_BOP as *mut u32, gpioa_bop);
        }
    }
}

struct Timer {}

impl Timer {
    fn init() -> Self {
        // Enable TIMER5 (simple timer)
        unsafe {
            let mut rcu_apb1en = read_volatile(RCU_APB1EN as *mut u32);
            // Set bit 2 to 1
            rcu_apb1en |= 0b1 << 4;
            write_volatile(RCU_APB1EN as *mut u32, rcu_apb1en);
        }
        // Configure timer in single-pulse mode
        unsafe {
            let mut timer5_ctl0 = read_volatile(TIMER5_CTL0 as *mut u32);
            timer5_ctl0 |= 0b1 << 3;
            write_volatile(TIMER5_CTL0 as *mut u32, timer5_ctl0);
        }
        // Enable update events
        unsafe {
            let mut timer5_ctl0 = read_volatile(TIMER5_CTL0 as *mut u32);
            timer5_ctl0 &= !(0b11 << 1);
            write_volatile(TIMER5_CTL0 as *mut u32, timer5_ctl0);
        }
        // Set prescaler to run PSC_CLK at 1 KHz
        // 8 MHz clock signal divided by value of TIMER5_PSC + 1
        unsafe {
            write_volatile(TIMER5_PSC as *mut u32, 7999 as u32);
        }

        Timer {}
    }

    fn delay(&self, millseconds: u16) {
        // Specify value to count up to
        unsafe {
            write_volatile(TIMER5_CAR as *mut u32, millseconds as u32);
        }
        // Reset interrupt flag
        unsafe {
            let mut timer5_intf = read_volatile(TIMER5_INTF as *mut u32);
            timer5_intf &= !(0b1);
            write_volatile(TIMER5_INTF as *mut u32, timer5_intf);
        }
        // Start counter
        unsafe {
            let mut timer5_ctl0 = read_volatile(TIMER5_CTL0 as *mut u32);
            timer5_ctl0 |= 0b1;
            write_volatile(TIMER5_CTL0 as *mut u32, timer5_ctl0);
        }
        // loop until interrupt flag is up
        loop {
            unsafe {
                let timer5_intf = read_volatile(TIMER5_INTF as *mut u32);
                // Check is bit 1 is 1
                if timer5_intf & 0b1 == 1 {
                    break;
                }
            }
        }
    }
}

#[allow(dead_code)]
#[entry]
fn run() -> ! {
    let timer = Timer::init();
    let green_led = GreenLed::init();

    loop {
        green_led.on();
        timer.delay(500);
        green_led.off();
        timer.delay(500);
    }
}
