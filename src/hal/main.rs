#![no_std]
#![no_main]

/*
Implements a blink app using the HAL (Hardware Abstraction layer)
The HAL for gd32vf103 is an implementation of embedded_hal
https://github.com/riscv-rust/gd32vf103xx-hal
https://docs.rs/gd32vf103xx-hal/0.4.0/gd32vf103xx_hal/index.html
*/

use panic_halt as _;
use riscv_rt::entry;

use gd32vf103xx_hal as hal;
use hal::{
    pac,
    prelude::*,
    timer::Timer,
    delay::Delay,
    
};
use embedded_hal::digital::v2::OutputPin;

// Main loop
#[entry]
fn run() -> ! {
    // Initialize
    let peripherals = pac::Peripherals::take().unwrap();
    let mut rcu = peripherals.RCU.configure().freeze();

    // Initialize timer
    // What is the effect of parameter `timeout` when timer is used as a delay provider?
    // Weird: the timer5 method https://docs.rs/gd32vf103xx-hal/0.4.0/src/gd32vf103xx_hal/timer.rs.html#27-39
    // Does not make use of the timeout parameter. However, setting it to 0 crashes the MCU
    let timer = Timer::timer5(peripherals.TIMER5, 1.hz(), &mut rcu);
    let mut delay = Delay::<pac::TIMER5>::new(timer);

    // Initialize pin
    let gpioa = peripherals.GPIOA.split(&mut rcu);
    let mut pa1 = gpioa.pa1.into_push_pull_output();
    // Note: I can't set frequency of pin to 2 MHz as I did in other examples.
    // into_push_pull_output() defaults to 50 MHz. 

    loop {
        pa1.set_low().unwrap();
        delay.delay_ms(1000 as u32);
        pa1.set_high().unwrap();
        delay.delay_ms(50 as u32);
    }
}
