#![no_std]
#![no_main]

/*
Implements a blink app using the PAC (Peripheral Access Crate)
benefits:
    * More readable
    * No need to manipulate bits
Issues (of this implementation):
    * The Peripherals struct must be passed around
    * Nothing is preventing the creation of multiple GreenLed or Timer instances
*/

use gd32vf103_pac as pac;
use panic_halt as _;
use riscv_rt::entry;

struct GreenLed {}

impl GreenLed {
    fn init(peripherals: &mut pac::Peripherals) -> Self {
        // Enable clock for GPIO port A
        peripherals.RCU.apb2en.write(|w| w.paen().set_bit());

        // Set the pin to output with push-pull
        peripherals
            .GPIOA
            .ctl0
            .write(|w| unsafe { w.ctl1().bits(0b00) });

        // Set the pin mode to output @ 2 MHz
        peripherals
            .GPIOA
            .ctl0
            .write(|w| unsafe { w.md1().bits(0b10) });

        // Return Self
        GreenLed {}
    }

    // Turn LED on. LED is turned on by setting the pin to low
    fn on(&self, peripherals: &mut pac::Peripherals) {
        // Toggle clear register bit for GPIOA1
        peripherals.GPIOA.bc.write(|w| w.cr1().set_bit());
    }

    // Turn LED off. LED is turned on by setting the pin to high
    fn off(&self, peripherals: &mut pac::Peripherals) {
        // Toggle operate register bit for GPIOA1
        peripherals.GPIOA.bop.write(|w| w.bop1().set_bit());
    }
}

struct Timer {}

impl Timer {
    fn init(peripherals: &mut pac::Peripherals) -> Self {
        // Enable TIMER5 (simple timer)
        peripherals.RCU.apb1en.write(|w| w.timer5en().set_bit());

        // Configure timer in single-pulse mode
        peripherals.TIMER5.ctl0.write(|w| w.spm().set_bit());

        // Enable update events
        peripherals.TIMER5.ctl0.write(|w| w.ups().clear_bit());
        peripherals.TIMER5.ctl0.write(|w| w.updis().clear_bit());

        // Set prescaler to run PSC_CLK at 1 KHz
        // 8 MHz clock signal divided by value of TIMER5_PSC + 1
        peripherals
            .TIMER5
            .psc
            .write(|w| unsafe { w.psc().bits(7999) });

        // Return Self
        Timer {}
    }

    fn delay(&self, peripherals: &mut pac::Peripherals, milliseconds: u16) {
        // Specify value to count up to
        peripherals
            .TIMER5
            .car
            .write(|w| unsafe { w.carl().bits(milliseconds) });

        // Reset interrupt flag
        peripherals.TIMER5.intf.write(|w| w.upif().clear_bit());

        // Start counter
        peripherals.TIMER5.ctl0.write(|w| w.cen().set_bit());

        // loop until interrupt flag is up
        loop {
            let intf_reg = peripherals.TIMER5.intf.read();
            if intf_reg.upif().bit_is_set() {
                break;
            }
        }
    }
}

#[entry]
fn run() -> ! {
    // Initialize
    let mut peripherals = pac::Peripherals::take().unwrap();

    let timer = Timer::init(&mut peripherals);
    let green_led = GreenLed::init(&mut peripherals);

    loop {
        green_led.on(&mut peripherals);
        timer.delay(&mut peripherals, 100);
        green_led.off(&mut peripherals);
        timer.delay(&mut peripherals, 100);
    }
}
