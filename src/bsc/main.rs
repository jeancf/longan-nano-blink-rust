#![no_std]
#![no_main]

/*
Implements a blink app using the BSC (Board Support crate)
https://github.com/riscv-rust/longan-nano
https://docs.rs/longan-nano/0.2.0/longan_nano/index.html
*/


use panic_halt as _;
use riscv_rt::entry;

use longan_nano::hal::{
    prelude::*,
    pac,
    timer::Timer,
    delay::Delay,
};
use longan_nano::led::*;

#[entry]
fn run() -> ! {
    // Initialize (same as with hal)
    let peripherals = pac::Peripherals::take().unwrap();
    let mut rcu = peripherals.RCU.configure().freeze();

    // Initialize timer (same as with hal)
    let timer = Timer::timer5(peripherals.TIMER5, 1.hz(), &mut rcu);
    let mut delay = Delay::<pac::TIMER5>::new(timer);

    // Initialize LED (longer than with hal)
    let gpioa = peripherals.GPIOA.split(&mut rcu);
    let gpioc = peripherals.GPIOC.split(&mut rcu);
    let (mut red, mut green, mut blue) = rgb(gpioc.pc13, gpioa.pa1, gpioa.pa2);
    // LEDs are on when initialized. Switch off the ones that we don't need
    red.off();
    blue.off();

    loop{
        green.on();
        delay.delay_ms(1000 as u32);
        green.off();
        delay.delay_ms(1000 as u32);
    }
}